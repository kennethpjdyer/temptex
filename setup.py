import pathlib
import re
from distutils.core import setup

packages = [ "temptex" ]
package_dirs = {}
for pkg in packages:
    src = re.sub("\.", "/", pkg)
    package_dirs[pkg] = src

bins = []
scripts = pathlib.Path("scripts")
for i in scripts.glob("*"):
    if i.is_file():
        bins.append(str(i))

setup(
    name="temptex",
    version="2022.1",
    packages=packages,
    package_dirs=package_dirs,
    scripts=bins)
