TempTeX
########

Simple CLI application for generating book and chapter LaTeX files from a template.
Designed to be used with Makefiles to ease a LaTeX workflow.
